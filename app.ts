// lib/app.ts
import express = require('express');
import logger from './utils/winston';

// Create a new express application instance
const app: express.Application = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  logger.info('Example app listening on port 3000!');
});